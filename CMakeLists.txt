﻿# CMakeList.txt : 최상위 CMake 프로젝트 파일, 전역 구성을 수행하고
# 여기에 하위 프로젝트를 포함합니다.
#
cmake_minimum_required (VERSION 3.8)

project ("DirectXTutorial")

set (DXSDK_PATH "C:\\Program Files (x86)\\Microsoft DirectX SDK (August 2009)")
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
	link_directories(${DXSDK_PATH}/Lib/x64)
elseif(CMAKE_SIZEOF_VOID_P EQUAL 4)
	link_directories(${DXSDK_PATH}/Lib/x86)
endif()
include_directories(${DXSDK_PATH}/Include)

# 하위 프로젝트를 포함합니다.
add_subdirectory ("Tutorial_Tut01_CreateDevice")
